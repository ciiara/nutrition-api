import { IsString, IsOptional, IsEnum } from "class-validator";
import { RecipeCategory } from "../../enums/recipe-category.enum";

export class QueryRecipeDTO {
    
    @IsString()
    @IsOptional()
    title?: string;

    @IsEnum(RecipeCategory)
    @IsOptional()
    category?: string;
}
