import { IsString, IsDefined, IsEnum, IsOptional, IsArray } from "class-validator";
import { CreateProductDTO } from "../product/create-product.dto";
import { RecipeCategory } from "../../enums/recipe-category.enum";
import { Type } from "class-transformer";
import { EditIngredientDTO } from "../ingredient/edit-ingredient.dto";
import { addSubrecipeDTO } from "./addSubrecipe.dto";

export class EditRecipeDTO {

    
    @IsOptional()
    @IsString()
    title?: string;

    @IsOptional()
    @Type( type => EditIngredientDTO )
    ingredients?: EditIngredientDTO[];

    @IsOptional()
    @IsArray()
    usedSubrecipesIds?: addSubrecipeDTO;

    @IsOptional()
    @IsEnum(RecipeCategory)
    category?: string;

    @IsOptional()
    recipiID?:string

    @IsOptional()
    description?:string
}
