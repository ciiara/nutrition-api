import { IsString, IsOptional, IsEnum, IsArray, IsNumber, IsBoolean, IsDate } from "class-validator";
import { RecipeCategory } from "../../enums/recipe-category.enum";
import { IngredientDTO } from "../ingredient/ingredient.dto";
import { addSubrecipeDTO } from "./addSubrecipe.dto";
import { Type } from "class-transformer";

export class RecipeDTO {
    @IsString()
    @IsOptional()
    id: string;

    @IsString()
    @IsOptional()
    title: string;

    @IsString()
    @IsOptional()
    information: string;

    @IsEnum(RecipeCategory)
    @IsOptional()
    categories: string;

    @Type( type => IngredientDTO)
    @IsOptional()
    ingredients: IngredientDTO[];

    @IsArray()
    @IsOptional()
    usedSubrecipesIds: addSubrecipeDTO;

    @IsNumber()
    @IsOptional()
    overallNutrition: number;

    @IsBoolean()
    @IsOptional()
    isBookmarked: boolean;

    @IsBoolean()
    @IsOptional()
    isArchived: boolean;

    @IsDate()
    @IsOptional()
    createdOn: Date;
}
