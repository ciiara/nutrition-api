import { IsString, IsDefined, IsEnum, IsArray, IsOptional, IsNumber } from 'class-validator';
import { Type } from 'class-transformer';
import { Recipe } from '../../../data/entities/recipe';
import { RecipeCategory } from '../../enums/recipe-category.enum';
import { CreateProductDTO } from '../product/create-product.dto';
import { addSubrecipeDTO } from './addSubrecipe.dto';

export class CreateRecipeDTO {
  @IsString()
  title: string;

  @IsOptional() @IsDefined()
  @Type( type => CreateProductDTO )
  ingredients?: CreateProductDTO[];

  @IsOptional() @IsArray()
  usedSubrecipesIds?: addSubrecipeDTO;

 @IsOptional() @IsEnum(RecipeCategory)
  category?: string;

  // @IsOptional()
  // energyTotal: number;

  @IsOptional() @IsString()
  information?: string;

@IsOptional() @IsNumber()
overallNutrition?: number;


}
