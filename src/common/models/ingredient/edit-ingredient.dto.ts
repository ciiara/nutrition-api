export class EditIngredientDTO{

    productName?:string

    id?:string

    isArchived?:boolean;

    amount?: number;

    selectedMeasure?: string;
    recipiID?
}