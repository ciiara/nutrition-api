import { IsNumber, IsString } from 'class-validator';
import { Type } from 'class-transformer';
import { ProductDTO } from '../product/product.dto';
import { isString } from 'util';

export class IngredientDTO {
  @Type( type => ProductDTO )
  product: ProductDTO;

  @IsNumber()
  amount: number;

  @IsString()
  selectedMeasure: string;
}
