import { IsNumber, IsString } from 'class-validator';
import { Type } from 'class-transformer';
import { Nutrition } from '../../../data/entities/nutrition';
import { Measure } from '../../../data/entities/measure';
import { Ingredient } from '../../../data/entities/ingredient';

export class ProductDTO {
  @IsNumber()
  code: number;

  @IsString()
  description: string;

  @IsString()
  foodGroup: string;

  @Type( type =>  Measure)
  measures: Measure[];

  @Type( type => Nutrition)
  nutrition: Nutrition;

  @Type( type => Ingredient)
  ingredients: Ingredient[];
}
