import { IsString, IsNumber, IsOptional } from 'class-validator';

export class CreateProductDTO {
  id?:string

  @IsString()
  productName: string;

  @IsNumber()
  amount: number;

  @IsString()
  @IsOptional()
  selectedMeasure?: string;

  recipiID?
}
