import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { User } from './common/decorators/user.decorator';

@Controller()
export class AppController {
  @Get()
  @UseGuards(AuthGuard())
  root(@User() authenticatedUser) {
    console.log(authenticatedUser);

    return {
      data: `Yay, you're logged in!`,
    };
  }

  @Get('/admin')
  @UseGuards(AuthGuard())
  admin(@User() authenticatedUser) {
    console.log(authenticatedUser);

    return {
      data: `Yay, you are an admin!`,
    };
  }
}
