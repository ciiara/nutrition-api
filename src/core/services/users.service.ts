import { Injectable, Post } from '@nestjs/common';
import { User } from '../../data/entities/user';
import { UserLoginDTO } from '../../common/models/user/user-login-dto';
import { JwtPayload } from '../interfaces/jwt-payload';
import { UserRegisterDTO } from '../../common/models/user/user-register-dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
//import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {}

  async signIn(user: UserLoginDTO): Promise<User | undefined> {
    const foundUser = await this.userRepository.findOne({
      where: {
        email: user.email,
      },
    });

    if (!foundUser) {
      return null;
    }
    const passCheck =user.password// await bcrypt.compare(user.password, foundUser.password);

    if (passCheck) {
      return foundUser;
    }
    return null;
  }

  async register(user: UserRegisterDTO): Promise<User | undefined> {
    user.password = user.password//await bcrypt.hash(user.password, 10);
    const savedUser = await this.userRepository.save({
      ...user,
    });

    return savedUser;
  }

  async validate(payload: JwtPayload): Promise<User | undefined> {
    return await this.userRepository.findOne({
      where: {
        ...payload,
      },
    });
  }
}
