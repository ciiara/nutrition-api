import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1564323279999 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `subrecipe` (`id` varchar(36) NOT NULL, `subrecipeName` varchar(255) NOT NULL, `amount` int NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `measures` (`id` varchar(36) NOT NULL, `measure` varchar(255) NOT NULL, `gramsPerMeasure` int NOT NULL, `productCode` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `nutritions` (`id` varchar(36) NOT NULL, `PROCNT` text NOT NULL, `FAT` text NOT NULL, `CHOCDF` text NOT NULL, `ENERC_KCAL` text NOT NULL, `SUGAR` text NOT NULL, `FIBTG` text NOT NULL, `CA` text NOT NULL, `FE` text NOT NULL, `P` text NOT NULL, `K` text NOT NULL, `NA` text NOT NULL, `VITA_IU` text NOT NULL, `TOCPHA` text NOT NULL, `VITD` text NOT NULL, `VITC` text NOT NULL, `VITB12` text NOT NULL, `FOLAC` text NOT NULL, `CHOLE` text NOT NULL, `productCode` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `products` (`code` int NOT NULL, `description` varchar(255) NOT NULL, `foodGroup` varchar(255) NOT NULL, PRIMARY KEY (`code`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `version` int NOT NULL, UNIQUE INDEX `IDX_97672ac88f789774dd47f7c8be` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `recipes` (`id` varchar(36) NOT NULL, `title` varchar(255) NOT NULL, `information` varchar(255) NOT NULL DEFAULT '', `categories` enum ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12') NOT NULL, `usedSubrecipesIds` text NOT NULL, `overallNutrition` int NOT NULL DEFAULT 0, `isBookmarked` tinyint NOT NULL DEFAULT 0, `isArchived` tinyint NOT NULL DEFAULT 0, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `version` int NOT NULL, `authorId` varchar(36) NULL, UNIQUE INDEX `IDX_aebbaa1cd38e2e5996d5847753` (`title`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `ingredients` (`id` varchar(36) NOT NULL, `amount` int NOT NULL DEFAULT 0, `isArchived` tinyint NOT NULL DEFAULT 0, `selectedMeasure` varchar(255) NULL DEFAULT null, `productCode` int NULL, `recipesId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `measures` ADD CONSTRAINT `FK_db5edcd1328fb776774cc41420e` FOREIGN KEY (`productCode`) REFERENCES `products`(`code`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `nutritions` ADD CONSTRAINT `FK_e94cf7b0ee06001c59bda599327` FOREIGN KEY (`productCode`) REFERENCES `products`(`code`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipes` ADD CONSTRAINT `FK_afd4f74f8df44df574253a7f37b` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `ingredients` ADD CONSTRAINT `FK_62805edc6999810ca7df35cc5ad` FOREIGN KEY (`productCode`) REFERENCES `products`(`code`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `ingredients` ADD CONSTRAINT `FK_38fbf5fe3936bd25f5bd6123554` FOREIGN KEY (`recipesId`) REFERENCES `recipes`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `ingredients` DROP FOREIGN KEY `FK_38fbf5fe3936bd25f5bd6123554`");
        await queryRunner.query("ALTER TABLE `ingredients` DROP FOREIGN KEY `FK_62805edc6999810ca7df35cc5ad`");
        await queryRunner.query("ALTER TABLE `recipes` DROP FOREIGN KEY `FK_afd4f74f8df44df574253a7f37b`");
        await queryRunner.query("ALTER TABLE `nutritions` DROP FOREIGN KEY `FK_e94cf7b0ee06001c59bda599327`");
        await queryRunner.query("ALTER TABLE `measures` DROP FOREIGN KEY `FK_db5edcd1328fb776774cc41420e`");
        await queryRunner.query("DROP TABLE `ingredients`");
        await queryRunner.query("DROP INDEX `IDX_aebbaa1cd38e2e5996d5847753` ON `recipes`");
        await queryRunner.query("DROP TABLE `recipes`");
        await queryRunner.query("DROP INDEX `IDX_97672ac88f789774dd47f7c8be` ON `users`");
        await queryRunner.query("DROP TABLE `users`");
        await queryRunner.query("DROP TABLE `products`");
        await queryRunner.query("DROP TABLE `nutritions`");
        await queryRunner.query("DROP TABLE `measures`");
        await queryRunner.query("DROP TABLE `subrecipe`");
    }

}
