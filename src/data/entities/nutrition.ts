import { INutrient } from './../../common/interfaces/nutrient';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne,
} from 'typeorm';
import { Product } from './product';

/**
 * Nutrition entity
 */
@Entity('nutritions')
export class Nutrition {
  /**
   * Id of the nutrition record
   */
  @PrimaryGeneratedColumn('uuid')
  id: string;
  /**
   * Product that has this nutrition
   */
  @ManyToOne(type => Product, product => product.nutrition, { nullable: true })
  product: Promise<Product>;
  /**
   * Nutrient
   */
  @Column('simple-json')
  PROCNT: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  FAT: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  CHOCDF: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  ENERC_KCAL: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  SUGAR: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  FIBTG: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  CA: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  FE: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  P: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  K: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  NA: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  VITA_IU: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  TOCPHA: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  VITD: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  VITC: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  VITB12: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  FOLAC: INutrient;
  /**
   * Nutrient
   */
  @Column('simple-json')
  CHOLE: INutrient;
}
