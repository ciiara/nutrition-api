import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    OneToMany,
    CreateDateColumn,
    UpdateDateColumn,
    VersionColumn,
    JoinTable,
    ManyToMany,
} from 'typeorm';

import { Ingredient } from './ingredient';
import { User } from './user';
import { RecipeCategory } from '../../common/enums/recipe-category.enum';
import { addSubrecipeDTO } from '../../common/models/recipe/addSubrecipe.dto';

/**
 * Recipe entity
 */
@Entity('recipes')
export class Recipe {
  /**
   * Id of the recipe
   */
  @PrimaryGeneratedColumn('uuid')
  id: string;
  /**
   * Title
   */
  @Column('nvarchar', {unique: true})
  title: string;
  /**
   * Additional Information
   */
  @Column('nvarchar', {default: ''})
  information: string;
  /**
   * Category - per client specification each recipe can have one category only
   */
  @Column({
    type: 'enum',
    enum: RecipeCategory,
    default: RecipeCategory['No category'],
  })
  categories: string;
  /**
   * Author of the recipe
   */
  @ManyToOne(type => User, user => user.recipes, { eager: true })
  author: User;
  /**
   * Products in the recipe
   */
  @OneToMany(type => Ingredient, ingredient => ingredient.recipes, { eager: true })
  ingredients: Ingredient[];
  /**
   * usedSubrecipesIds is either null or contains an array with all the recipeIDs for used subrecipes
   */
  @Column('simple-json')
  usedSubrecipesIds: addSubrecipeDTO;
  /**
   * Overall nutrition data for the recipe will be calculated per creation of the recipe
   */
  @Column({default: 0})
  overallNutrition: number;
  /**
   * Recipe energy per 100g
   */
  // @Column({default: 0})
  // energyTotal: number;
  /**
   * Archive Status
   */
  @Column({default: false})
  isBookmarked: boolean;
  /**
   * Archive Status
   */
  @Column({default: false})
  isArchived: boolean;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @VersionColumn()
  version: number;

  // @Column({ type: 'varchar' ,default: null})
  // description: string;
}
