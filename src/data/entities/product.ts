import { Ingredient } from './ingredient';
import { Entity, PrimaryColumn, Column,  OneToMany } from 'typeorm';
import { Measure } from './measure';
import { Nutrition } from './nutrition';

/**
 * Product entity
 */
@Entity('products')
export class Product {
  /**
   * Code of the product
   */
  @PrimaryColumn()
  code: number;
  /**
   * Description of the product
   */
  @Column('nvarchar')
  description: string;
  /**
   * Food group to which the product belongs
   */
  @Column()
  foodGroup: string;
  /**
   * Available measures of the product
   */
  @OneToMany(type => Measure, measure => measure.product, { eager: true })
  measures: Measure[];
  /**
   * Nutrient data for the product
   */
  @OneToMany(type => Nutrition, nutrition => nutrition.product, { eager: true })
  nutrition: Nutrition;
  /**
   * Recipe product
   */
  @OneToMany(type => Ingredient, ingredient => ingredient.product)
  ingredients: Promise<Ingredient[]>;
}
