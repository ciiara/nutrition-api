import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  VersionColumn,
  OneToMany,
} from 'typeorm';
import { Recipe } from './recipe';

/**
 * User entity
 */
@Entity('users')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  name: string;

  /**
   * @password is saved in the DB encrypted
   */
  @Column('nvarchar')
  password: string;

  /**
   * @email is used for login of the user
   */
  @Column({ unique: true })
  email: string;

  @OneToMany(type => Recipe, recipe => recipe.author)
  recipes: Promise<Recipe>;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @VersionColumn()
  version: number;
}
