import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany } from "typeorm";
import { Recipe } from "./recipe";

@Entity('subrecipe')
export class AddSubrecipe {

    
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    subrecipeName:string

    @Column()
    amount:number




}