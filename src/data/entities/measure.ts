import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne,
} from 'typeorm';
import { Product } from './product';

/**
 * Measure entity
 */
@Entity('measures')
export class Measure {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  /**
   * Product that has this measure
   */
  @ManyToOne(type => Product, product => product.measures)
  product: Promise<Product>;
  /**
   * Measure type
   */
  @Column()
  measure: string;
  /**
   * Grams per measure
   */
  @Column()
  gramsPerMeasure: number;
}
