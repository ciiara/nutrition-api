import { Product } from './product';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Recipe } from './recipe';

/**
 * Product in a recipe entity
 */
@Entity('ingredients')
export class Ingredient {
  /**
   * Id of the product
   */
  @PrimaryGeneratedColumn('uuid')
  id: string;
  /**
   * Product that is the ingredient based on
   */
  @ManyToOne(type => Product, product => product.ingredients, { eager: true })
  product: Product;
  /**
   * Amount of the product
   */
  @Column({ default: 0 })
  amount: number;
  /**
   * Recipes using the product
   */
  @ManyToOne(type => Recipe, recipe => recipe.ingredients)
  recipes: Promise<Recipe>;
  /**
   * Archive Status
   */
  @Column({default: false})
  isArchived: boolean;
  /**
   * measure selected for ingredient
   */
  @Column({default: null})
  selectedMeasure: string;
}
