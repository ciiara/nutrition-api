import 'reflect-metadata';
import { createConnection } from 'typeorm';
//import * as bcrypt from 'bcrypt';
import * as _foodGroup from '../database/fd_group.json';
import * as _foodDescription from '../database/food_des.json';
import * as _nutrientData from '../database/nut_data.json';
import * as _nutrientDefinition from '../database/nutr_def.json';
import * as _weight from '../database/weight.json';

import { User } from '../entities/user';
import { Measure } from '../entities/measure';
import { Nutrition } from '../entities/nutrition';
import { Product } from '../entities/product';

// Custom async forEach
async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

const main = async () => {
  const connection = await createConnection();

  const userRepository = connection.manager.getRepository(User);
  const productRepository = connection.manager.getRepository(Product);
  const measureRepository = connection.manager.getRepository(Measure);
  const nutritionRepository = connection.manager.getRepository(Nutrition);

  const foodGroup = _foodGroup;
  const foodDescription = _foodDescription;
  const nutrientData = _nutrientData as any;
  const nutrientDefinition = _nutrientDefinition;
  const weight = _weight;

  const populateDatabase = async () => {
    await asyncForEach(foodDescription, async (p) => {
      const product = await productRepository.create();
      product.code = p.NDB_No;
      product.description = p.Long_Desc;
      const productFoodGroup = foodGroup.find((g) => g.FdGrp_Cd === p.FdGrp_Cd);
      product.foodGroup = productFoodGroup.FdGrp_desc;
      await productRepository.save(product);

      const measures = weight.filter((wght) => wght.NDB_No === p.NDB_No);
      await asyncForEach(measures, async (m) => {
        const measure = new Measure();
        measure.measure = m.Msre_Desc;
        let amount = m.Amount;
        if (m.Msre_Desc.includes('package') && m.Amount !== '1') {
          amount = 1;
        }
        measure.gramsPerMeasure = amount * m.Gm_Wgt;
        measure.product = Promise.resolve(product);
        await measureRepository.save(measure);
      });

      const nutrition = new Nutrition();
      nutrientDefinition.forEach((n) => {
        const code = n.Nutr_no;
        const nutriData = nutrientData.find((nutr) => (+nutr.Nutr_No === code && +nutr.NDB_No === p.NDB_No));
        let value;
        if (nutriData === undefined) {
          value = 0;
        } else {
          value = nutriData.Nutr_Val;
        }

        nutrition[n.Tagname] = {
          description: n.NutrDesc,
          unit: n.Units,
          value,
        };
      });
      nutrition.product = Promise.resolve(product);
      //nutrition.recipeNutrition = Promise.resolve(null);
      await nutritionRepository.save(nutrition);
    });

    const martin = await userRepository.findOne({
      where: {
        name: 'Martin',
      },
    });

    // if (!martin) {
    //   const user = new User();
    //   user.name = 'Martin';
    //   const passwordHash = await bcrypt.hash('Aaaa!0', 10);
    //   user.password = passwordHash;
    //   user.email = 'mar@mar.mar';
    //   await userRepository.save(user);
    // } else {
    //   console.log(`Martin is already in the db`);
    // }

    // const silvia = await userRepository.findOne({
    //   where: {
    //     name: 'Silvia',
    //   },
    // });

    // if (!silvia) {
    //   const user = new User();
    //   user.name = 'Silvia';
    //   const passwordHash = await bcrypt.hash('Aaaa!0', 10);
    //   user.password = passwordHash;
    //   user.email = 'mail@mail.com';
    //   await userRepository.save(user);
    // } else {
    //   console.log(`Silvia is already in the db`);
    // }

    // const testUser = await userRepository.findOne({
    //   where: {
    //     firstName: 'Test',
    //   },
    // });

    // if (!testUser) {
    //   const user = new User();
    //   user.name = 'TestUser';
    //   const passwordHash = await bcrypt.hash('Aaaa!0', 10);
    //   user.password = passwordHash;
    //   user.email = 'test@test.test';
    //   await userRepository.save(user);
    // } else {
    //   console.log(`TestUser is already in the db`);
    // }

    connection.close();
  };

  populateDatabase();
};

main().catch(console.error);
