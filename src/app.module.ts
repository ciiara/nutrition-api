import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { CoreModule } from './core/core.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from './config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from './config/config.service';
import { ProductModule } from './features/product/product.module';
import { RecipeModule } from './features/recipe/recipe.module';
import { IngredientModule } from './features/recipe/ingredient/ingredient.module';

@Module({
  imports: [
    CoreModule,
    AuthModule,
    ConfigModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        type: configService.dbType as any,
        host: configService.dbHost,
        port: configService.dbPort,
        username: configService.dbUsername,
        password: configService.dbPassword,
        database: configService.dbName,
        entities: ['./src/data/entities/*.ts'],
      }),
    }),
    ProductModule,
    RecipeModule,
    IngredientModule
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
