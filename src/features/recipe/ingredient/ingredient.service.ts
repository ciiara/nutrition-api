import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Ingredient } from '../../../data/entities/ingredient';
import { Repository, Equal } from 'typeorm';
import { Product } from '../../../data/entities/product';
import { EditIngredientDTO } from '../../../common/models/ingredient/edit-ingredient.dto';

@Injectable()
export class IngredientService {
    constructor(
        @InjectRepository(Ingredient)
        private readonly ingredientRepository: Repository<Ingredient>,
        @InjectRepository(Product)
        private readonly productRepository: Repository<Product>,
    ) {}

    async createIngredient(ingredientName: string, ingredientAmount: number, selectedMeasure: string, recipeID?: any): Promise<Ingredient> {
        const newIngredient = new Ingredient();

        newIngredient.product = await this.productRepository.findOne(
            {
                where: { description: Equal(ingredientName) },
                relations: ['nutrition', 'measures'],
            },
        );

        if (!!newIngredient.product) {
            newIngredient.product.nutrition = await newIngredient.product.nutrition;
            newIngredient.product.measures = await newIngredient.product.measures;
            newIngredient.amount = ingredientAmount;
            newIngredient.selectedMeasure = selectedMeasure;

            if (recipeID != null) {
                newIngredient.recipes = recipeID;
            }
            // console.log(newIngredient);

            return await this.ingredientRepository.save(newIngredient);
        } else {
            throw new NotFoundException(`Product does not exsist`);
        }
    }

    async editIngredient(id:string,editIngredient:EditIngredientDTO):Promise<Ingredient>{
     const targetedIngredient:Ingredient = await this.ingredientRepository.findOne({
            where: {
                id: id,
                isArchived: false,
            },
        });
        console.log(editIngredient.selectedMeasure);
        
        
        //if(!!targetedIngredient){
            targetedIngredient.amount=editIngredient.amount
            //targetedIngredient.isArchived=editIngredient.isArchived
            targetedIngredient.selectedMeasure=editIngredient.selectedMeasure
            console.log(targetedIngredient);
            await this.ingredientRepository.save(targetedIngredient)
            return targetedIngredient
        //}else{
            return
        //}
    }

    async deleteIngredient(id:string):Promise<Ingredient>{
        const targetedIngredient:Ingredient = await this.ingredientRepository.findOne({
            where: {
                id: id,
                isArchived: false,
            },
        });
        if(!!targetedIngredient){
        targetedIngredient.isArchived=true
        this.ingredientRepository.save(targetedIngredient)
        return targetedIngredient
        }else{
            throw new NotFoundException(`Product does not exsist`);
        }
        
    }
}
