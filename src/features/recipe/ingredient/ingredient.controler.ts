import { Controller, UseGuards, Delete, Param } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { IngredientService } from "./ingredient.service";

@UseGuards(AuthGuard())
@Controller('/ingredient')
export class IngredientController {

    constructor(
        private readonly ingredientService: IngredientService,
    ) {}

    @Delete('/:id')
    async deleteIngredient(
        @Param('id') id: string,
    ): Promise<any> {
        console.log(id);
        return await this.ingredientService.deleteIngredient(id);
    }

}
