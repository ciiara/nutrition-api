import { Module } from '@nestjs/common';
import { IngredientService } from './ingredient.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Ingredient } from '../../../data/entities/ingredient';
import { Product } from '../../../data/entities/product';
import { Measure } from '../../../data/entities/measure';
import { IngredientController } from './ingredient.controler';
import { AuthModule } from '../../../auth/auth.module';

@Module({
  controllers: [IngredientController],
  imports: [
    TypeOrmModule.forFeature([Ingredient, Product, Measure]),
    AuthModule
  ],
  providers: [IngredientService],
  exports: [IngredientService],
})
export class IngredientModule {}
