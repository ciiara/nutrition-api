import { Controller, Req, Body, Post, ValidationPipe, UseGuards, Get, Param, Query, Delete, Put } from '@nestjs/common';
import { Recipe } from '../../data/entities/recipe';
import { RecipeService } from './recipe.service';
import { CreateRecipeDTO } from '../../common/models/recipe/create-recipe.dto';
import { AuthGuard } from '@nestjs/passport';
import { QueryRecipeDTO } from '../../common/models/recipe/query-recipe.dto';
import { EditRecipeDTO } from '../../common/models/recipe/edit-recipe.dto';
import { addSubrecipeDTO } from '../../common/models/recipe/addSubrecipe.dto';

@UseGuards(AuthGuard())
@Controller('/recipe')
export class RecipeController {
    constructor(
        private readonly recipesService: RecipeService,
    ) {}

    /* todo : use RecipeDTO as return object instead; everywhere */

    @Post()
    async createRecipe(
        @Body(new ValidationPipe({ whitelist: true, transform: true }))
        recipe: CreateRecipeDTO,
        @Req() request: any,
    ): Promise<Recipe> {
        
        return await this.recipesService.createRecipe(recipe, request.user);
    }

    @Get()
    async getRecipe(
        @Query() query: QueryRecipeDTO,
        @Req() request: any,
    ): Promise<Recipe[]> {
        return await this.recipesService.getRecipes(query);
    }

    @Get('bookmarks')
    async getBookmarkedRecipe(
        @Req() request: any,
    ): Promise<Recipe[]> { // todo : use RecipeDTO instead
        return await this.recipesService.getBookmarkedRecipes();
    }

    @Put('/:id')
    async editRecipe(
        @Param('id') id: string,
        @Body(new ValidationPipe({ whitelist: true, transform: true }))
        editInfo: EditRecipeDTO,
    ): Promise<Recipe> {
        return await this.recipesService.editRecipe(id, editInfo);
    }

    @Delete('/:id')
    async deleteRecipe(
        @Param('id') id: string,
    ): Promise<{}> {
        return await this.recipesService.deleteRecipe(id);
    }

    @Get('/:title')
    async getSingleRecipe(
        @Param('title') title: string,
        @Req() request: any,
    ): Promise<Recipe> {
        console.log(title);
        
        return await this.recipesService.getSingleRecipes(title);
    }

    @Get('/id/:id')
    async getRecipeById(
        @Param('id') id: string,
    ): Promise<{}> {
        return await this.recipesService.getRecipeById(id);
    }

    @Put('bookmark/:id')
    async bookmarkRecipe(
        @Param('id') id: string,
    ): Promise<Recipe> {  // todo : use RecipeDTO instead
        return await this.recipesService.toggleBookmarkRecipe(id);
    }

    @Post('add')
    async addSubrecipe(
        @Body()
        addSubrecipe:addSubrecipeDTO,
    ): Promise<Recipe> {
        return await this.recipesService.addSubrecipe(addSubrecipe)
    }
}
