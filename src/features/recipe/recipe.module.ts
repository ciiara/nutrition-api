import { Module } from '@nestjs/common';
import { RecipeController } from './recipe.controller';
import { RecipeService } from './recipe.service';
import { IngredientModule } from './ingredient/ingredient.module';
import { IngredientService } from './ingredient/ingredient.service';
import { Recipe } from '../../data/entities/recipe';
import { Product } from '../../data/entities/product';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../../auth/auth.module';

@Module({
  controllers: [RecipeController],
  providers: [RecipeService, IngredientService],
  imports: [
    TypeOrmModule.forFeature([Recipe, Product]),
    IngredientModule,
    AuthModule,
  ],
})
export class RecipeModule {}
