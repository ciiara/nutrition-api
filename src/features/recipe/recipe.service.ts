import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Recipe } from '../../data/entities/recipe';
import { Repository, Equal, Like } from 'typeorm';
import { IngredientService } from './ingredient/ingredient.service';
import { User } from '../../data/entities/user';
import { CreateRecipeDTO } from '../../common/models/recipe/create-recipe.dto';
import { CreateProductDTO } from '../../common/models/product/create-product.dto';
import { QueryRecipeDTO } from '../../common/models/recipe/query-recipe.dto';
import { EditRecipeDTO } from '../../common/models/recipe/edit-recipe.dto';
import { addSubrecipeDTO } from '../../common/models/recipe/addSubrecipe.dto';
import { IngredientDTO } from '../../common/models/ingredient/ingredient.dto';
import { Nutrition } from '../../data/entities/nutrition';
import { EditIngredientDTO } from '../../common/models/ingredient/edit-ingredient.dto';
import { Ingredient } from '../../data/entities/ingredient';
import { ClassTransformer } from 'class-transformer';
import { RecipeDTO } from '../../common/models/recipe/recipe.dto';

@Injectable()
export class RecipeService {
    constructor(
        @InjectRepository(Recipe)
        private readonly recipeRepository: Repository<Recipe>,
        private readonly ingredientService: IngredientService,
        // private readonly ingredientRepository: Repository<Ingredient>,
    ) {}

    /**
     * @param recipe consists of title, ingredients, recipes, category data
     * @param author is the User entity with data about the logged in user
     */
    async createRecipe(recipe: CreateRecipeDTO, author: User): Promise<Recipe> { // todo : use RecipeDTO instead
        const ingredients: CreateProductDTO[] = recipe.ingredients.length > 0 ? (await recipe.ingredients) : [];

        const ingredientsAsEntities = await Promise.all(ingredients.map( async (ingredient: CreateProductDTO) => {
            return await this.ingredientService.createIngredient(ingredient.productName, ingredient.amount, ingredient.selectedMeasure);
        }));

        const newRecipe = new Recipe();
        newRecipe.author = await author;
        newRecipe.categories = recipe.category;
        newRecipe.ingredients = ingredientsAsEntities;
        newRecipe.ingredients = await newRecipe.ingredients;

        // let energyTotal = 0;
        // newRecipe.ingredients.map(
        //     (ingredient): void => {
        //       const ingredientAmount: number = ingredient.amount;
        //       const selectedMeasure: string = ingredient.selectedMeasure; // todo
        //       const productNutrition: Nutrition = ingredient.product.nutrition[0];
        //       const kcalVal = productNutrition.ENERC_KCAL.value;  // in kcal - data per 100g

        //       energyTotal += kcalVal / 100 * (ingredientAmount * 1); // todo : add selected measure, for now it's set to 1
        //     },
        //   );

         newRecipe.information = recipe.information;

        newRecipe.usedSubrecipesIds = recipe.usedSubrecipesIds;
        newRecipe.title = recipe.title;
        newRecipe.overallNutrition = recipe.overallNutrition;

        return await this.recipeRepository.save(newRecipe);
    }

    /**
     * @param query searches the DB for recipes containing the given title or category
     * @expects recipe titles to be unique in our DB
     */
    async getRecipes(query: QueryRecipeDTO): Promise<Recipe[]> { // todo : use RecipeDTO instead
        const searchedTitle = query.title ? query.title : '';
        const searchedCategory = query.category ? query.category : '';

        if (searchedTitle) {
            const foundRecipes = await this.recipeRepository.find(
                {
                    where: {
                        title: Equal(searchedTitle),
                        isArchived: false,
                    },
                },
            );

            if (foundRecipes.length < 1) {
                throw new NotFoundException('Sorry, no recipe with this title was found.');
            }

            return foundRecipes;
        }

        if (searchedCategory) {
            const foundRecipes = await this.recipeRepository.find({
                    where: {
                        title: Like(searchedCategory),
                        isArchived: false,
                    },
            });

            if (foundRecipes.length < 1) {
                throw new NotFoundException('Sorry, no such recipe in this category was found.');
            }

            return foundRecipes;
        }

        return await this.recipeRepository.find({
            where: {
                isArchived: false,
            },
            order: {
                title: 'ASC',
            },
        });
    }

    async editRecipe(recipeID: string, editInfo: EditRecipeDTO): Promise<Recipe> {
        const targetedRecipe = await this.recipeRepository.findOne({
            where: {
                id: recipeID,
                isArchived: false,
            },
        });
        let ingredients=editInfo.ingredients
        if (!targetedRecipe) {
            throw new NotFoundException('Oops, we cant edit this recipe since we cant find it.');
        }
        if (!!editInfo.category) {
            targetedRecipe.categories = editInfo.category;
        }
        
        //if (!!editInfo.ingredients) {
           
            
            await ingredients.map((x: EditIngredientDTO)=>{
               if(x.id != undefined){
                return this.ingredientService.editIngredient(x.id,x);}

               this.ingredientService.createIngredient(x.productName,x.amount,x.selectedMeasure,editInfo.recipiID)
               }) // todo
        
               //targetedRecipe.description=editInfo.description
               targetedRecipe.usedSubrecipesIds=editInfo.usedSubrecipesIds
               
        return await this.recipeRepository.save(targetedRecipe);
    }

    /**
     * @param recipeID contains the id of the recipe that we want to delete
     */
    async deleteRecipe(recipeID: string): Promise<{}> {
        const targetedRecipe = await this.recipeRepository.findOne({
            where: {
                id: recipeID,
                isArchived: Equal(false),
            },
        });

        if (!!targetedRecipe) {
            targetedRecipe.isArchived = true;
            this.recipeRepository.save(targetedRecipe);

            return {message: 'You successfully deleted the recipe.'};
        } else {
            throw new NotFoundException(`Sorry, recipe with id ${recipeID} was found.`);
        }
    }

    async getSingleRecipes(title:string):Promise<Recipe>{
        const targetedRecipe = await this.recipeRepository.findOne({
            where: {
                title: title,
                isArchived: Equal(false),
            },
        });

        const temp=targetedRecipe.ingredients.map(x=>{if(x.isArchived===false){return x}})
        targetedRecipe.ingredients=temp
        console.log(targetedRecipe);
        
        if(!targetedRecipe.isArchived){
            return targetedRecipe
        }else{
           
            throw new NotFoundException(`Sorry, the recipe you are looking for is deleted or does not exsist.`); 
        }

    }

    async getRecipeById(id: string): Promise<Recipe> {
        const targetedRecipe = await this.recipeRepository.findOne({
            where: {
                id,
                isArchived: Equal(false),
            },
        });

        return targetedRecipe;
    }

    async addSubrecipe(addSubrecipe:addSubrecipeDTO):Promise<any>{
        // const masterRecipe:Recipe= await this.getSingleRecipes(addSubrecipe.subrecipeName)
        // const subrecipe:Recipe= await this.getSingleRecipes(addSubrecipe.amount)
        // masterRecipe.usedSubrecipesIds=[]
        // masterRecipe.usedSubrecipesIds.push(subrecipe.id)
        // return masterRecipe
    }

    /* Bookmarks functionality */
    async getBookmarkedRecipes(): Promise<Recipe[]> { // todo : use RecipeDTO instead
        return await this.recipeRepository.find({
            where: {
                isArchived: false,
                isBookmarked: true,
            },
            order: {
                title: 'ASC',
            },
        });
    }

    async toggleBookmarkRecipe(id: string): Promise<Recipe> { // todo : use RecipeDTO instead
        let targetedRecipe = await this.recipeRepository.findOne({
            where: {
                id: Equal(id),
                isArchived: false,
            },
        });

        if (targetedRecipe) {
            if (targetedRecipe.isBookmarked === true) {
                targetedRecipe.isBookmarked = false;
            } else if (targetedRecipe.isBookmarked === false) {
                targetedRecipe.isBookmarked = true;
            } else {
                targetedRecipe.isBookmarked = null;
            }
        }

        targetedRecipe = await this.recipeRepository.save(targetedRecipe);
        return targetedRecipe;
    }
}
