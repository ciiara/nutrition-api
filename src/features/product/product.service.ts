import { Injectable, NotFoundException } from '@nestjs/common';
import { Product } from '../../data/entities/product';
import { Repository, Like } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class ProductService {
    constructor(
        @InjectRepository(Product)
        private readonly productRepository: Repository<Product>,
    ) {}

    /**
     * @param productDesc if no product description query is provided we assign '' and search the DB for every existing product
     */
    async getProductByDescription(productDesc: string): Promise<Product[]> {
        const productDescription = productDesc ? productDesc : '';

        const foundProducts = await this.productRepository.find({
            where: {
                description: Like(`%${productDescription}%`),
            },
        });

        if (foundProducts.length < 1) {
            throw new NotFoundException('Sorry, no such product was found.');
        }

        return await foundProducts;
    }

    async getProductFoodgroup(foodgroup:string): Promise<any>{
        let foundProducts:any = await this.productRepository.find({
            where: {
                foodGroup: foodgroup,
            },
        });
        if (foundProducts.length < 1) {
            throw new NotFoundException('Sorry, no such group was found.');
        }
        return await foundProducts;
    }






}
