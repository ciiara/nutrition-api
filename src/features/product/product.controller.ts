import { Controller, Get, Query } from '@nestjs/common';
import { Product } from '../../data/entities/product';
import { ProductService } from './product.service';

@Controller('product')
export class ProductController {
    constructor(
        private readonly productService: ProductService,
    ) {}

    @Get()
    async getProductByDescription(
        @Query('productDescription') productDescription: string,
    ): Promise<Product[]> {
        return await this.productService.getProductByDescription(productDescription);
    }

    @Get('foodgroup')
    async getProductByFoodgroup(
        @Query('foodgroup') foodgroup: string,
    ): Promise<any> {
        return await this.productService.getProductFoodgroup(foodgroup);
    }


}
