# RECIPE NUTRITION CALCULATOR API



## Description

Final Project Assignment for Telerik Academy Alpha with JavaScript - Design and implement single-page web application that will allow restaurant chefs to create and manage recipes composed of products with known nutrition values. During the interaction with the system users should see changes in nutrition information in real time. Newly created recipes could be tagged with a category from a list.


### User Stories - Project Timeline

- x Authenticate users - Register, Login
- x Users can CRUD recipes
- x Users can search list of recipes by name or filter by category or by chosen nutrition value between given boundaries
- x Users can search for product and filter by product group
- x Users can bookmark recipes


### Stack

- Database - MySQL
- REST API - NestJS

----------

# Getting started

## Installation

Clone the repository
   `git clone https://gitlab.com/ciiara/nutrition-api.git`

Switch to the repo folder
   `cd nutrition-api`
    
Install dependencies   
   `npm install`

----------

## Database

### MySQL

Create a new MySQL database with the name `nutrition_db` (or the name you specified in the ormconfig.json)

MySQL database settings are in ormconfig.json

Start local MySQL server and create new database `nutrition_db`

In order to create the tables run 
    `npm run typeorm -- migration:run`
Initialize data
    `npm run seed`

After these steps you should be ready to run the application with `npm start`.

* In order to create recipes, etc., you'd have to be logged in (Login / register functionality is available)
* The JWT token used for authentication is set to expire in one hour

## NPM scripts

- `npm start` - Start application
- `npm run start:dev` - Start application in nodemon
- `npm run typeorm` - run Typeorm commands
- `npm run seed` - initial seed for the database
- `npm run test` - run Jest test runner 
- `npm run compodoc` - run Compodoc to see full documentation of the app 

----------

# Support

This is a one off project no continues support what so ever

# Authors

Silvia Velikova,
Martin Nedyalkov

----------

# Postman Collection

A Postman collection can be found on the top level of our files directory
Look for a folder named 'postman'

# Usage Paths

## For users:
`post  "/register"`=Will register user

Request body:
`
{
	"name": string,
	"email": string,
	"password": string
}
`

`post  "/login"`=Will give you user login.

Request body:
`
{
	"email": string,
	"password": string
}
`

## For Recipes:

`GET "/recipe"`=Will give you all recipes

Optional request body for filtering:

Request body:
`{
title?: string,
category?: string
}
`

`POST "/recipe"`=Will create recipe

Request body:
`
{
	"title": string,
	"ingredients": array,
	"usedSubrecipesIds": array,
	"category": number,
	description?:string,
}
`

`PUT "/recipe/:recipeID"`=Will edit recipe

Request body:

`{
title?: string,
ingredients?:array,
usedSubrecipesIds?:array,
category?: string,
recipiID?:string,
description?:string
}`

`DELETE "/recipe/:recipeID"`=Will delete recipe by ID

`GET "/recipe/:title"`=Will return recipe by title

`PUT "/recipe/'bookmark/:id"`=Will bookmark a recipe by ID

## For Ingredients:
`DELETE "/ingredient/:id"`=Will delete ingredient by ID 

## For Products:
`GET "/product"`=Will return a product/s based on description

Request Query:
`{productDescription: string}`

`GET "/product/foodgroup"`=Will filter products by foodgroup

Request Query:
`{foodgroup: string}`
